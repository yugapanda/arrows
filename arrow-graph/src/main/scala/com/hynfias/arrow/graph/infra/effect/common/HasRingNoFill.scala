package com.hynfias.arrow.graph.infra.effect.common

import com.hynfias.arrow.graph.infra.effect.tempo.child.RingNoFill
import processing.core.PApplet

trait HasRingNoFill {

  var ringNoFillChildren: List[RingNoFill] = Nil

  def ringNoFillDraw( x: Float, y: Float)(implicit p: PApplet): Unit = {

    if(p.frameCount % 20 == 0) {
      ringNoFillChildren =  RingNoFill(x.toInt, y.toInt, 0, 0, 5) :: ringNoFillChildren
    }

    ringNoFillChildren.foreach(x => {
      x.draw(p)
      x.update()
    })
    ringNoFillChildren = ringNoFillChildren.filter(x => x.isLiving)
  }



}
